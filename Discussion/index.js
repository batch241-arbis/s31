// Node.js introduction

// Check node version on terminal using: node --version
/*
	- Use the "require" directive to load Node.js modules
	- A "module" is a software component or part of a program that contains one or more routines
	- The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol.
	- The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications.
	- HTTP is a protocol that allows the fetching of resources such as HTML documents.
	- Clients (browser) and servers communicate by exchanging individual messages.
	- The messages sent by the client, usually a web browser are called requests.
	- The messages sent by the server as an answer are called responses.
*/
let http = require("http");


// Using this module, createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client.
// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server.
// The arguments passed in the function are request and response objects (data type) that contains methods that allows us to receive requests from the client and responses back to it.
http.createServer(function (request, response) {

	//Use the writeHead() method to:
		// Set a status code for the responses. 200 means OK.
		// Set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Send the response with a text content 'Hello World'.
	response.end('Hello World');


// A port is a virutal point where network connection starts and end.
// Each port is associated with a specific process or service.
// The server will be assigned to port 4000 via ".listen(4000)" method where the server will listen to any requests that are sent to it, eventually communicating with our server.
}).listen(4000);


console.log('Server running at localhost:4000');